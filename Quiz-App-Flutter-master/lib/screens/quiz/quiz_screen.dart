import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz_app/controllers/question_controller.dart';
import 'package:quiz_app/models/Questions.dart';
import 'package:quiz_app/screens/welcome/welcome_screen.dart';

import 'components/body.dart';

class QuizScreen extends StatefulWidget {
  List<Question> allQuestions;

  QuizScreen({this.allQuestions});

  @override
  _QuizScreenState createState() => _QuizScreenState(allQuestions);
}

class _QuizScreenState extends State<QuizScreen> {
  List<Question> allQuestions;
  List<bool> _isChecked = [];
  List<bool> Ans = [];

  _QuizScreenState(this.allQuestions);

  @override
  void initState() {
    // TODO: implement initState
    //goi api lay ve doi tuong device => ve do thi
    print('all ques');
    print(allQuestions);
    print(allQuestions[0].question);
    super.initState();
    _isChecked = List<bool>.filled(12, false);
    Ans = List<bool>.filled(3, false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Widget build(BuildContext context) {
    QuestionController _controller = Get.put(QuestionController());
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // Fluttter show the back button automatically
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          FlatButton(onPressed: _controller.nextQuestion, child: Text("Skip")),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            (allQuestions != null)
                ? Expanded(
                    child: ListView.builder(
                      itemCount: allQuestions.length,
                      itemBuilder: (context, i) {
                        return Container(
                          decoration: new BoxDecoration(color: Colors.black),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(allQuestions[i].id.toString() +
                                            ". " +
                                            allQuestions[i].question +
                                            ".", style: TextStyle(fontSize: 18),),
                                      ),
                                      // SizedBox(
                                      //   width: 10,
                                      // ),
                                      // Expanded(
                                      //     child: Text(
                                      //         allQuestions[i].question + '.')),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(30, 0, 30, 0),
                                child: Container(
                                  decoration: new BoxDecoration(
                                      color: Colors.indigoAccent),
                                  height: 250,
                                  child: ListView.builder(
                                      itemCount: 4,
                                      itemBuilder: (context, j) {
                                        return CheckboxListTile(
                                          title:
                                              Text(allQuestions[i].answers[j]),
                                          value: _isChecked[4 * i + j],
                                          onChanged: (val) {
                                            setState(
                                              () {
                                                if (val == true) {
                                                  // print('select= '+j.toString());
                                                  if (j ==
                                                      allQuestions[i]
                                                          .correctAnswer) {
                                                    print('true');
                                                    Ans[i] = true;
                                                  }
                                                } else {
                                                  Ans[i] = false;
                                                }
                                                // print('true value= '+allQuestions[i].correctAnswer.toString());
                                                // print('select= '+j.toString());
                                                // print('val= '+val.toString());
                                                _isChecked[4 * i + j] = val;
                                                // print(_isChecked[4*i+j].toString());
                                                print(Ans);
                                              },
                                            );
                                          },
                                        );
                                        //   Container(
                                        //   child: Text(allQuestions[i].answers[j]),
                                        // );
                                      }),
                                ),
                              ),

                              // Text(allQuestions[i].answers[0]),
                              // Text(allQuestions[i].answers[1]),
                              // Text(allQuestions[i].answers[2]),
                              // Text(allQuestions[i].answers[3]),

                              // ListView.builder(
                              //     itemCount: 4,
                              //     itemBuilder: (context,j){
                              //       return Container(
                              //         height: 50,
                              //         child: Text(allQuestions[i].answers[j]),
                              //       );
                              //     }
                              // )
                              SizedBox(height: 30,)
                            ],
                          ),
                        );
                      },
                    ),
                  )
                : Container(
                    height: 0,
                    width: 0,
                  ),
            RaisedButton(
              child: Text('SUBMIT'),
              textColor: Colors.white,
              onPressed: () {
                showAlertDialog(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => WelcomeScreen()),
        );
      },
    );
    // set up the buttons
    Widget checkRankedButton = TextButton(
      child: Text("Ranked"),
      onPressed: () {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Go to Ranked"),
        ));
      },
    );
    Widget createNewQuizButton = TextButton(
      child: Text("New Quiz"),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => WelcomeScreen()),
        );
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Result"),
      content: Text("Correct: " +
          Ans.where((item) => item == true).length.toString() +
          "/" +
          Ans.length.toString()),
      actions: [
        checkRankedButton,
        createNewQuizButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
