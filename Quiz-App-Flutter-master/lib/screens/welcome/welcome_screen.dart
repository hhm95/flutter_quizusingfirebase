
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:quiz_app/constants.dart';
import 'package:quiz_app/models/Questions.dart';
import 'package:quiz_app/screens/quiz/quiz_screen.dart';

class WelcomeScreen extends StatelessWidget {

  List<Question> allQuestions = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/icons/bgmanh.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child:
          // SvgPicture.asset("assets/icons/bg.svg", fit: BoxFit.fill),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(flex: 2), //2/6
                  Text(
                    "Let's Play Quiz,",
                    style: Theme.of(context).textTheme.headline4.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),

                  Spacer(), // 1/6
                  Text("Enter your name:"),
                  SizedBox(height: 10,),
                  TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xFF1C2341),
                      hintText: "Full Name",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      ),
                    ),
                  ),
                  Spacer(), // 1/6
                  InkWell(
                    onTap: () => GetData(context),//Get.to(QuizScreen()),
                    child: Container(
                      width: double.infinity,
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(kDefaultPadding * 0.75), // 15
                      decoration: BoxDecoration(
                        gradient: kPrimaryGradient,
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      ),
                      child: Text(
                        "Lets Start Quiz",
                        style: Theme.of(context)
                            .textTheme
                            .button
                            .copyWith(color: Colors.black),
                      ),
                    ),
                  ),
                  Spacer(flex: 2),
                  // it will take 2/6 spaces
                ],
              ),
            ),
          ),

      ),
    );
  }

  void GetData(BuildContext context)async {



    print('getdata');
    // var querySnapshot = await FirebaseFirestore.instance.collection('quiz').snapshots();
    var collection = FirebaseFirestore.instance.collection('quiz');
    // print(collection);
    var querySnapshot = await collection.get();
    // print(querySnapshot);
    // print(querySnapshot.docs.length);
    for (var queryDocumentSnapshot in querySnapshot.docs) {
      // print(queryDocumentSnapshot);
      print(queryDocumentSnapshot.data());
      Map<String, dynamic> data = queryDocumentSnapshot.data();
      var question = Question.fromJson(data);
      allQuestions.add(question);
      // aQuestion.question = question;
      // aQuestion.answer = correctAnswer;
      // aQuestion.options = answer;
      // print(aQuestion);
      // allQuestions.add(aQuestion);


    }
    // print(allQuestions);
    if(allQuestions.length > 0) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => QuizScreen(allQuestions: allQuestions)),
      );
    }
  }
}
