
class Question {
  final int id, correctAnswer;
  final String question;
  final List<String> answers;

  Question({this.id, this.question, this.correctAnswer, this.answers});

  factory Question.fromJson(Map<String, dynamic> json) => Question(
    id: json["id"],
    correctAnswer: json['correctAnswer'],
    question: json['question'],
    answers: List<String>.from(json["answers"].map((x) => x)),
  );

  // Question.fromSnapshot(DataSnapshot snapshot, this.id) :
  //       // id = snapshot.key,
  //       answer = snapshot.value["answer"],
  //       question = snapshot.value["question"],
  //       options = snapshot.value["options"];
  //
  // toJson() {
  //   return {
  //     "answer": answer,
  //     "question": question,
  //     "options": options,
  //   };
  // }
}

const List sample_data = [
  {
    "id": 1,
    "question":
        "Flutter is an open-source UI software development kit created by ______",
    "options": ['Apple', 'Google', 'Facebook', 'Microsoft'],
    "answer_index": 1,
  },
  {
    "id": 2,
    "question": "When google release Flutter.",
    "options": ['Jun 2017', 'Jun 2017', 'May 2017', 'May 2018'],
    "answer_index": 2,
  },
  {
    "id": 3,
    "question": "A memory location that holds a single letter or number.",
    "options": ['Double', 'Int', 'Char', 'Word'],
    "answer_index": 2,
  },
  {
    "id": 4,
    "question": "What command do you use to output data to the screen?",
    "options": ['Cin', 'Count>>', 'Cout', 'Output>>'],
    "answer_index": 2,
  },
];
